public class Board{
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
	}
	public String toString(){
		String filler = "";
		for(int i =0; i < closedTiles.length; i++){
			//closedTiles[sum-1]==true
			if(!closedTiles[i]){
				filler=filler+(i+1)+" ";
			}
			else{
			 filler=filler+"X"+" ";
			}
		}
		return filler;
	}
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		int sum = die1.getPips()+die2.getPips();
		//closedTiles[sum-1]==true
		if(!closedTiles[sum-1]){
			closedTiles[sum-1] = true;
			System.out.println("Closing tile:"+sum);
			return false;
		}
		else{
			System.out.println("This position is already shut");
			return true;
		}
	}
}
import java.util.Scanner;
public class ShutTheBox{
public static void main(String[]args){
	Scanner reader = new Scanner(System.in);
	Board game = new Board();
	boolean gameOver = false;
	System.out.println("Welcome to the jungle, we have fun and games");
	
	int player1Wins=0;
	int player2Wins=0;
	//gameOver==true
	while(!gameOver){
		System.out.println("Player 1's turn");
		System.out.println(game);
		//game.playATurn==true
		if(game.playATurn()){
			System.out.println("Player 2 won!");
			player2Wins++;
			System.out.println("Enter yes to play again and no to end it");
			
			String rewind=reader.next();
			if(rewind.equals("yes")){
				game= new Board();
				continue;
			}
			System.out.println("Player 1 won "+player1Wins+" times and player 2 won "+player2Wins+" times");
			gameOver = true;
			}
		else{
			System.out.println("Player 2's turn");
			System.out.println(game);
			
			if(game.playATurn()){
			System.out.println("Player 1 won!");
			player1Wins++;
			System.out.println("Enter yes to play again and no to end it");
			
			String rewind=reader.next();
			if(rewind.equals("yes")){
				game= new Board();
				continue;
			}
			System.out.println("Player 1 won "+player1Wins+" times and player 2 won "+player2Wins+" times");
			gameOver = true;
			}
		}
	}
}
}
	
	